---
title: Download
menu:
  main:
    weight: 4
---

+ Windows 10 - available only as [portable 64 bit](https://github.com/KDE/ghostwriter/releases/latest)
+ [Linux repos](#linux)
+ MacOS -[build from source](#macos) (community-supported)

+ [Get the source!](https://invent.kde.org/office/ghostwriter)

## Linux

Enter the following commands based on your Linux distribution.

<table class="table align-top">
    <thead>
        <th scope="col">Distro</th>
        <th scope="col">Installation Commands</th>
    </thead>
    <tr class="align-top">
        <td class="align-top">Ubuntu</td>
        <td class="align-top">
        <div class="terminal"><pre>$ sudo add-apt-repository ppa:wereturtle/ppa
$ sudo apt update
$ sudo apt install ghostwriter</pre></div>
        </td>
    </tr>
    <tr class="align-top">
        <td class="align-top">Fedora</td>
        <td class="align-top"><div class="terminal"><pre>$ sudo dnf copr enable wereturtle/stable
$ sudo dnf install ghostwriter</code></div></td>
    </tr>
</table>

## MacOS

MacOS has unofficial support provided by the community.
The app mostly works but for a few quirks.  Unfortunately, you
will have to compile the source in order to run it, as there is
currently no installer available.

Download the source code from the
[GitLab repository](https://invent.kde.org/office/ghostwriter) and see
the README.md file for build instructions.

If you would like to contribute to improving MacOS support,
please see the [contributing guidelines](/contribute).
